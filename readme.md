# Consumer Loan Backend Application

## Code coverage
Code coverage is reported on every test run, so to check code coverage just run all tests:

`./gradlew clean test`

Code coverage report is generated in the following directory:

`build/reports/jacoco/test/html/`

and can be checked by opening the `index.html` in the browser.