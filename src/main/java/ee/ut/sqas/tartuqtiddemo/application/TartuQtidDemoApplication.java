package ee.ut.sqas.tartuqtiddemo.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan(basePackages = {"ee.ut.sqas.tartuqtiddemo.repository"})
@SpringBootApplication
public class TartuQtidDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TartuQtidDemoApplication.class, args);
	}

}
