package ee.ut.sqas.tartuqtiddemo.controller;

import ee.ut.sqas.tartuqtiddemo.repository.LoanApplication;
import ee.ut.sqas.tartuqtiddemo.repository.LoanRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.Optional;

@Controller
@RequestMapping(value = "/test")
public class TestController {

  @Resource
  private LoanRepository loanRepository;

  @GetMapping
  public LoanApplication getLoan() {
    Optional<LoanApplication> loan = loanRepository.findById(1);
    return loan.orElseThrow(() -> new RuntimeException("Not found"));
  }
}
