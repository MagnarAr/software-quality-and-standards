package ee.ut.sqas.tartuqtiddemo.service.consumerloan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ConsumerLoanDto {
    private String fullName;
    private String idCode;
    private String idCodeIssuer;
    private String address;
    private String phoneNumber;
    private String email;
    private Long loanAmount;
    private int loanPeriodInYears;
}
