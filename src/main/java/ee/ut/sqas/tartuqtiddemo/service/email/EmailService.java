package ee.ut.sqas.tartuqtiddemo.service.email;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

@Service
public class EmailService {

    @Resource
    private JavaMailSender mailSender;

    public void sendLoanConfirmationMessage(String to, String subject, String messageContent) {
        validateInput(to, subject, messageContent);
        mailSender.send(createMailMessage(to, subject, messageContent));
    }

    private void validateInput(String to, String subject, String messageContent) {
        boolean isValid =
                !StringUtils.isEmpty(to)
                        && !StringUtils.isEmpty(subject)
                        && !StringUtils.isEmpty(messageContent);
        if (!isValid) {
            throw new EmailException("Invalid input data");
        }
    }

    private SimpleMailMessage createMailMessage(String to, String subject, String messageContent) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(messageContent);
        return message;
    }
}
