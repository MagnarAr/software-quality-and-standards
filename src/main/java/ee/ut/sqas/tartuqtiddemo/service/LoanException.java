package ee.ut.sqas.tartuqtiddemo.service;

public class LoanException extends RuntimeException {
    public LoanException(String message) {
        super(message);
    }
}
