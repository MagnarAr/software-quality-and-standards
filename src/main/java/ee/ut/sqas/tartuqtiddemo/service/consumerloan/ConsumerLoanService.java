package ee.ut.sqas.tartuqtiddemo.service.consumerloan;

import ee.ut.sqas.tartuqtiddemo.repository.LoanApplication;
import ee.ut.sqas.tartuqtiddemo.repository.LoanRepository;
import ee.ut.sqas.tartuqtiddemo.service.LoanException;
import ee.ut.sqas.tartuqtiddemo.service.email.EmailService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;


/**
 * Covers backend service requirements associated with Tartu Qtid USE CASE #4
 */
@Service
public class ConsumerLoanService {

    @Resource
    private LoanRepository loanRepository;
    @Resource
    private EmailService emailService;

    /**
     * @param consumerLoanDto
     * @return Loan application unique ID
     */
    public int applyForConsumerLoan(ConsumerLoanDto consumerLoanDto) {
        validatePersonalData(consumerLoanDto);
        validateLoanAmount(consumerLoanDto);
        validateLoanPeriod(consumerLoanDto);
        int applicationId = saveAndGetApplicationId(consumerLoanDto);
        if (applicationId > 0) {
            sendEmail(consumerLoanDto);
        }
        return applicationId;
    }

    private void validatePersonalData(ConsumerLoanDto consumerLoanDto) {
        boolean isValid =
                !StringUtils.isEmpty(consumerLoanDto.getFullName())
                        && !StringUtils.isEmpty(consumerLoanDto.getFullName())
                        && !StringUtils.isEmpty(consumerLoanDto.getIdCode())
                        && !StringUtils.isEmpty(consumerLoanDto.getIdCodeIssuer())
                        && !StringUtils.isEmpty(consumerLoanDto.getAddress())
                        && !StringUtils.isEmpty(consumerLoanDto.getPhoneNumber())
                        && !StringUtils.isEmpty(consumerLoanDto.getEmail());
        if (!isValid) {
            throw new LoanException("Invalid personal data");
        }
    }

    private void validateLoanAmount(ConsumerLoanDto consumerLoanDto) {
        int maxAmount = 10_000_000;
        int minAmount = 1000;
        boolean isValid = consumerLoanDto.getLoanAmount() != null
                && consumerLoanDto.getLoanAmount() >= minAmount
                && consumerLoanDto.getLoanAmount() <= maxAmount;
        if (!isValid) {
            throw new LoanException("Invalid amount");
        }
    }

    private void validateLoanPeriod(ConsumerLoanDto consumerLoanDto) {
        int maxPeriod = 20;
        int minPeriod = 1;
        boolean isValid = consumerLoanDto.getLoanPeriodInYears() >= minPeriod
                && consumerLoanDto.getLoanPeriodInYears() <= maxPeriod;
        if (!isValid) {
            throw new LoanException("Invalid period");
        }
    }

    private int saveAndGetApplicationId(ConsumerLoanDto consumerLoanDto) {
        LoanApplication application = loanRepository.save(mapToEntity(consumerLoanDto));
        return application.getId();
    }

    private LoanApplication mapToEntity(ConsumerLoanDto consumerLoanDto) {
        return LoanApplication.builder()
                .fullName(consumerLoanDto.getFullName())
                .idCode(consumerLoanDto.getIdCode())
                .idCodeIssuer(consumerLoanDto.getIdCodeIssuer())
                .address(consumerLoanDto.getAddress())
                .phoneNumber(consumerLoanDto.getPhoneNumber())
                .email(consumerLoanDto.getEmail())
                .loanAmount(consumerLoanDto.getLoanAmount())
                .loanPeriodInYears(consumerLoanDto.getLoanPeriodInYears())
                .build();
    }

    private void sendEmail(ConsumerLoanDto consumerLoanDto) {
        String subject = "We have received your consumer loan application";
        String message = "We have received your application. Come get your money pls.";
        emailService.sendLoanConfirmationMessage(consumerLoanDto.getEmail(), subject, message);
    }
}
