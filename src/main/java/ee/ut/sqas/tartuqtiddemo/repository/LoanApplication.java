package ee.ut.sqas.tartuqtiddemo.repository;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class LoanApplication {
    @Id
    @GeneratedValue
    private int id;
    private String fullName;
    private String idCode;
    private String idCodeIssuer;
    private String address;
    private String phoneNumber;
    private String email;
    private Long loanAmount;
    private int loanPeriodInYears;
}
