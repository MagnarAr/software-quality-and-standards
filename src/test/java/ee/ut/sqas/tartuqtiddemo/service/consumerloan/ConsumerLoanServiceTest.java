package ee.ut.sqas.tartuqtiddemo.service.consumerloan;

import ee.ut.sqas.tartuqtiddemo.repository.LoanApplication;
import ee.ut.sqas.tartuqtiddemo.repository.LoanRepository;
import ee.ut.sqas.tartuqtiddemo.service.LoanException;
import ee.ut.sqas.tartuqtiddemo.service.email.EmailService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class ConsumerLoanServiceTest {

    @Mock
    private LoanRepository loanRepository;
    @Mock
    private EmailService emailService;
    @InjectMocks
    private ConsumerLoanService consumerLoanService;

    @Captor
    private ArgumentCaptor<LoanApplication> loanApplicationArgumentCaptor;

    private ConsumerLoanDto validConsumerLoanDto;

    @BeforeEach
    void setUp() {
        validConsumerLoanDto = ConsumerLoanDto.builder()
                .fullName("Test Test")
                .idCode("123456789")
                .idCodeIssuer("EE")
                .address("Tartu, Testaddress 1-23")
                .phoneNumber("+37212345678")
                .email("test@testmail.ee")
                .loanAmount(12345L)
                .loanPeriodInYears(5)
                .build();
    }

    @Test
    void validApplicationShouldReturnInsertedId() {
        LoanApplication loanApplication = LoanApplication.builder()
                .id(1)
                .build();
        when(loanRepository.save(any())).thenReturn(loanApplication);
        int applicationId = consumerLoanService.applyForConsumerLoan(validConsumerLoanDto);
        assertEquals(loanApplication.getId(), applicationId);

        verify(loanRepository, times(1)).save(loanApplicationArgumentCaptor.capture());
        LoanApplication savedApplication = loanApplicationArgumentCaptor.getValue();
        assertEquals(savedApplication.getFullName(), "Test Test");
        assertEquals(savedApplication.getIdCode(), "123456789");
        assertEquals(savedApplication.getIdCodeIssuer(), "EE");
        assertEquals(savedApplication.getAddress(), "Tartu, Testaddress 1-23");
        assertEquals(savedApplication.getPhoneNumber(), "+37212345678");
        assertEquals(savedApplication.getEmail(), "test@testmail.ee");
        assertEquals(savedApplication.getLoanAmount(), 12345L);
        assertEquals(savedApplication.getLoanPeriodInYears(), 5);
    }

    @Test
    void invalidLoanDataThrowsException() {
        ConsumerLoanDto invalidConsumerLoan = new ConsumerLoanDto();
        assertThrows(LoanException.class,
                () -> consumerLoanService.applyForConsumerLoan(invalidConsumerLoan)
        );
    }

    @Test
    void missingPersonalDataThrowsException() {
        ConsumerLoanDto invalidConsumerLoan = validConsumerLoanDto;
        invalidConsumerLoan.setFullName(null);
        LoanException loanException = assertThrows(LoanException.class,
                () -> consumerLoanService.applyForConsumerLoan(invalidConsumerLoan)
        );
        assertEquals("Invalid personal data", loanException.getMessage());
    }

    @Test
    void biggerThanMaxPeriodThrowsException() {
        ConsumerLoanDto invalidConsumerLoan = validConsumerLoanDto;
        invalidConsumerLoan.setLoanPeriodInYears(21);
        LoanException loanException = assertThrows(LoanException.class,
                () -> consumerLoanService.applyForConsumerLoan(invalidConsumerLoan)
        );
        assertEquals("Invalid period", loanException.getMessage());
    }

    @Test
    void lessThanMinPeriodThrowsException() {
        ConsumerLoanDto invalidConsumerLoan = validConsumerLoanDto;
        invalidConsumerLoan.setLoanPeriodInYears(0);
        LoanException loanException = assertThrows(LoanException.class,
                () -> consumerLoanService.applyForConsumerLoan(invalidConsumerLoan)
        );
        assertEquals("Invalid period", loanException.getMessage());
    }

    @Test
    void biggerThanMaxAmountThrowsException() {
        ConsumerLoanDto invalidConsumerLoan = validConsumerLoanDto;
        invalidConsumerLoan.setLoanAmount(10_000_001L);
        LoanException loanException = assertThrows(LoanException.class,
                () -> consumerLoanService.applyForConsumerLoan(invalidConsumerLoan)
        );
        assertEquals("Invalid amount", loanException.getMessage());
    }

    @Test
    void lessThanMinAmountThrowsException() {
        ConsumerLoanDto invalidConsumerLoan = validConsumerLoanDto;
        invalidConsumerLoan.setLoanAmount(999L);
        LoanException loanException = assertThrows(LoanException.class,
                () -> consumerLoanService.applyForConsumerLoan(invalidConsumerLoan)
        );
        assertEquals("Invalid amount", loanException.getMessage());
    }

    @Test
    void confirmationMailIsSentOnValidInput() {
        LoanApplication loanApplication = LoanApplication.builder()
                .id(1)
                .build();
        when(loanRepository.save(any())).thenReturn(loanApplication);
        doNothing().when(emailService).sendLoanConfirmationMessage(any(), any(), any());
        consumerLoanService.applyForConsumerLoan(validConsumerLoanDto);
        verify(emailService, times(1)).sendLoanConfirmationMessage(any(), any(), any());
    }
}