package ee.ut.sqas.tartuqtiddemo.service.consumerloan;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;

class ConsumerLoanDtoTest {

  @Test
  void testSetters() {
    ConsumerLoanDto consumerLoanDto = new ConsumerLoanDto();

    consumerLoanDto.setAddress("address");
    assertThat(consumerLoanDto.getAddress()).isEqualTo("address");

    consumerLoanDto.setEmail("email");
    assertThat(consumerLoanDto.getEmail()).isEqualTo("email");

    consumerLoanDto.setFullName("full name");
    assertThat(consumerLoanDto.getFullName()).isEqualTo("full name");

    consumerLoanDto.setIdCode("id code");
    assertThat(consumerLoanDto.getIdCode()).isEqualTo("id code");

    consumerLoanDto.setIdCodeIssuer("EE");
    assertThat(consumerLoanDto.getIdCodeIssuer()).isEqualTo("EE");

    consumerLoanDto.setLoanAmount(36000L);
    assertThat(consumerLoanDto.getLoanAmount()).isEqualTo(36000L);

    consumerLoanDto.setLoanPeriodInYears(3);
    assertThat(consumerLoanDto.getLoanPeriodInYears()).isEqualTo(3);

    consumerLoanDto.setPhoneNumber("+372123456");
    assertThat(consumerLoanDto.getPhoneNumber()).isEqualTo("+372123456");
  }

  @Test
  void testBuilder() {
    ConsumerLoanDto.ConsumerLoanDtoBuilder consumerLoanDtoBuilder = ConsumerLoanDto.builder()
        .fullName("Test Test")
        .idCode("123456789")
        .idCodeIssuer("EE")
        .address("Tartu, Testaddress 1-23")
        .phoneNumber("+37212345678")
        .email("test@testmail.ee")
        .loanAmount(12345L)
        .loanPeriodInYears(5);

    String toStringValue = consumerLoanDtoBuilder.toString();

    assertThat(toStringValue).isEqualTo("ConsumerLoanDto.ConsumerLoanDtoBuilder(fullName=Test Test, idCode=123456789,"
                                            + " idCodeIssuer=EE, address=Tartu, Testaddress 1-23, "
                                            + "phoneNumber=+37212345678, email=test@testmail.ee, loanAmount=12345, "
                                            + "loanPeriodInYears=5)");
  }
}