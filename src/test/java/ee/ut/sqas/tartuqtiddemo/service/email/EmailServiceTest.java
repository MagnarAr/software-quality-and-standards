package ee.ut.sqas.tartuqtiddemo.service.email;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class EmailServiceTest {

    @Mock
    private JavaMailSender mailSender;
    @InjectMocks
    private EmailService emailService;

    @BeforeEach
    void setUp() {
    }

    private String validToAddress = "test@test.ee";
    private String validSubject = "Subject: you got mail!";
    private String validMessage = "... Message Message Message Message Message ...";


    @Test
    void invalidToAddressThrowException() {
        assertThrows(EmailException.class,
                () -> {
                    emailService.sendLoanConfirmationMessage(null, validSubject, validMessage);
                }
        );
    }

    @Test
    void invalidMessageThrowException() {
        assertThrows(EmailException.class,
                () -> {
                    emailService.sendLoanConfirmationMessage(validToAddress, validSubject, "");
                }
        );
    }

    @Test
    void invalidSubjectThrowException() {
        assertThrows(EmailException.class,
                () -> {
                    emailService.sendLoanConfirmationMessage(validToAddress, "", validMessage);
                }
        );
    }

    @Test
    void validEmailIsSent() {
        doNothing().when(mailSender).send(any(SimpleMailMessage.class));
        emailService.sendLoanConfirmationMessage(validToAddress, validSubject, validMessage);
        verify(mailSender, times(1)).send(any(SimpleMailMessage.class));
    }
}