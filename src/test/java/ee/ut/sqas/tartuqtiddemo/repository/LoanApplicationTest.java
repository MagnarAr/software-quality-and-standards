package ee.ut.sqas.tartuqtiddemo.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class LoanApplicationTest {

  @Test
  void testSetters() {
    LoanApplication loanApplication = new LoanApplication();
    loanApplication.setId(1);
    assertThat(loanApplication.getId()).isEqualTo(1);

    loanApplication.setAddress("address");
    assertThat(loanApplication.getAddress()).isEqualTo("address");

    loanApplication.setEmail("email");
    assertThat(loanApplication.getEmail()).isEqualTo("email");

    loanApplication.setFullName("full name");
    assertThat(loanApplication.getFullName()).isEqualTo("full name");

    loanApplication.setIdCode("id code");
    assertThat(loanApplication.getIdCode()).isEqualTo("id code");

    loanApplication.setIdCodeIssuer("EE");
    assertThat(loanApplication.getIdCodeIssuer()).isEqualTo("EE");

    loanApplication.setLoanAmount(36000L);
    assertThat(loanApplication.getLoanAmount()).isEqualTo(36000L);

    loanApplication.setLoanPeriodInYears(3);
    assertThat(loanApplication.getLoanPeriodInYears()).isEqualTo(3);

    loanApplication.setPhoneNumber("+372123456");
    assertThat(loanApplication.getPhoneNumber()).isEqualTo("+372123456");
  }

  @Test
  void testBuilder() {
    LoanApplication.LoanApplicationBuilder loanApplicationBuilder = LoanApplication.builder()
        .fullName("Test Test")
        .idCode("123456789")
        .idCodeIssuer("EE")
        .address("Tartu, Testaddress 1-23")
        .phoneNumber("+37212345678")
        .email("test@testmail.ee")
        .loanAmount(12345L)
        .loanPeriodInYears(5);
    String toStringValue = loanApplicationBuilder.toString();

    assertThat(toStringValue).isEqualTo("LoanApplication.LoanApplicationBuilder(id=0, fullName=Test Test, "
                                            + "idCode=123456789, idCodeIssuer=EE, address=Tartu, Testaddress 1-23, "
                                            + "phoneNumber=+37212345678, email=test@testmail.ee, loanAmount=12345, "
                                            + "loanPeriodInYears=5)");
  }
}